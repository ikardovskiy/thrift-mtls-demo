Simple Thrift example with mTLS
===================================

Slightly modified
https://thrift.apache.org/tutorial/java.html

JavaClient
                TSSLTransportParameters params = new TSSLTransportParameters();
                params.setTrustStore("client-truststore.pfx", "thrift", "SunX509", "JKS");
                params.setKeyStore("client-keystore.pfx", "thrift", "SunX509", "JKS");
                /*
                 * Get a client transport instead of a server transport. The connection is opened on
                 * invocation of the factory method, no need to specifically call open()
                 */
                transport = TSSLTransportFactory.getClientSocket("localhost", 9091, 0, params);

JavaServer
            TSSLTransportParameters params = new TSSLTransportParameters();
            // The Keystore contains the private key
            params.setKeyStore("server-keystore.pfx", "thrift", null, null);
            params.setTrustStore("server-truststore.pfx", "thrift", null, null);
            params.requireClientAuth(true);

            TServerTransport serverTransport = TSSLTransportFactory.getServerSocket(9091, 0, null, params);
            TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));

Build
===================
$ mvn package

Start server
======================
$ java -cp target\MTlsDemo-0.0.1-SNAPSHOT-jar-with-dependencies.jar JavaServer

Client without TLS
======================
$ java -cp target\MTlsDemo-0.0.1-SNAPSHOT-jar-with-dependencies.jar JavaClient simple

Client with mTLS
======================
$ java -cp target\MTlsDemo-0.0.1-SNAPSHOT-jar-with-dependencies.jar JavaClient secure

Client with mTLS and TLS handshake logging
===============================================
$ java -Djavax.net.debug=ssl,handshake -cp target\MTlsDemo-0.0.1-SNAPSHOT-jar-with-dependencies.jar JavaClient secure

search log for "Produced client Certificate message" and "Consuming server Certificate handshake message"
    javax.net.ssl|DEBUG|01|main|2021-03-02 12:13:42.931 MSK|CertificateMessage.java:1122|Produced client Certificate message (
    "Certificate": {
      "certificate_request_context": "",
      "certificate_list": [
      {
        "certificate" : {
          "version"            : "v3",
          "serial number"      : "37 45 65 F6",
          "signature algorithm": "SHA256withRSA",
          "issuer"             : "CN=ThriftClient",
          "not before"         : "2021-03-02 11:53:09.000 MSK",
          "not  after"         : "2031-02-28 11:53:09.000 MSK",
          "subject"            : "CN=ThriftClient",
          "subject public key" : "RSA",

    javax.net.ssl|DEBUG|01|main|2021-03-02 12:13:42.869 MSK|CertificateMessage.java:1154|Consuming server Certificate handshake message (
    "Certificate": {
      "certificate_request_context": "",
      "certificate_list": [
      {
        "certificate" : {
          "version"            : "v3",
          "serial number"      : "1F 57 A0 0B",
          "signature algorithm": "SHA256withRSA",
          "issuer"             : "CN=ThriftServer",
          "not before"         : "2021-03-02 11:48:51.000 MSK",
          "not  after"         : "2031-02-28 11:48:51.000 MSK",
          "subject"            : "CN=ThriftServer",
          "subject public key" : "RSA",

Create server keystore store
===============================
$ del server-keystore.pfx
$ keytool -genkeypair -keyalg RSA -keysize 2048 -keypass thrift -alias server  -keystore server-keystore.pfx -storepass thrift -storetype JKS -dname "CN=ThriftServer" -validity 3650
$ keytool -list -v  -keystore server-keystore.pfx -storepass thrift
$ keytool -export -rfc -file server.crt -alias server  -keystore server-keystore.pfx -storepass thrift -storetype JKS
$ notepad server.crt
$ server.crt

Create client keystore store
===============================
$ del client-keystore.pfx
$ keytool -genkeypair -keyalg RSA -keysize 2048 -keypass thrift -alias client  -keystore client-keystore.pfx -storepass thrift -storetype JKS -dname "CN=ThriftClient" -validity 3650
$ keytool -list -v  -keystore client-keystore.pfx -storepass thrift
$ keytool -export -rfc -file client.crt -alias client  -keystore client-keystore.pfx -storepass thrift -storetype JKS
$ notepad client.crt
$ client.crt

Create client truststore
=========================
Create client truststore
$ del client-truststore.pfx
$ keytool -import -trustcacerts -file server.crt -alias server  -keystore client-truststore.pfx -storepass thrift -storetype JKS
$ keytool -list -v  -keystore client-truststore.pfx -storepass thrift
$ del server.crt

Create server truststore
=========================
Create server truststore
$ del server-truststore.pfx
$ keytool -import -trustcacerts -file client.crt -alias client  -keystore server-truststore.pfx -storepass thrift -storetype JKS
$ keytool -list -v  -keystore server-truststore.pfx -storepass thrift
$ del client.crt

t\Example002-0.0.1-SNAPSHOT-jar-with-dependencies.jar JavaClient secure